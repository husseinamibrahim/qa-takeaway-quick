package com.justeat.androiduitechtest.tests

import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.rule.ActivityTestRule
import com.justeat.androiduitechtest.MainActivity
import com.justeat.androiduitechtest.pages.AddTaskPopUpRobot
import com.justeat.androiduitechtest.pages.MainPageRobot
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * All test logic is separate from the page object in order to be able to use all page objects for different tests
 */

class AddTaskTest {
    @Rule
    @JvmField
    val mActivityRule = ActivityTestRule(MainActivity::class.java, false, false)

    val mainPageRobot = MainPageRobot()
    val addTaskPopUpRobot = AddTaskPopUpRobot()

    @Before
    fun launchActivity(){
        mActivityRule.launchActivity(null)
    }

    @Test
    fun addTaskTest(){
        mainPageRobot.toolBarText.check(matches(isDisplayed()))
        mainPageRobot.clickOnAddBtn()
        addTaskPopUpRobot.typeTextInTextView()
        addTaskPopUpRobot.clickOnAddBtn()
        mainPageRobot.addedTaskItem.check(matches(isDisplayed()))
    }

}