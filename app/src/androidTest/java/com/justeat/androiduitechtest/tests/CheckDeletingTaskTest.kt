package com.justeat.androiduitechtest.tests

import android.support.test.espresso.NoMatchingViewException
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.rule.ActivityTestRule
import com.justeat.androiduitechtest.MainActivity
import com.justeat.androiduitechtest.pages.CompleteDeleteTaskPopUpRobot
import com.justeat.androiduitechtest.pages.MainPageRobot
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class CheckDeletingTaskTest {
    @Rule
    @JvmField
    val mActivityRule = ActivityTestRule(MainActivity::class.java, false, false)

    val addTaskTest = AddTaskTest()
    val completeDeleteTaskPopUpRobot = CompleteDeleteTaskPopUpRobot()
    val mainPageRobot = MainPageRobot()

    @Before
    fun launchActivity(){
        mActivityRule.launchActivity(null)
    }

    @Test
    fun checkDeletingTasksAdded(){
        addTaskTest.addTaskTest()
        mainPageRobot.addedTaskItem.perform(click())
        completeDeleteTaskPopUpRobot.deleteTaskBtn.check(matches(isDisplayed()))
        completeDeleteTaskPopUpRobot.deleteTaskBtn.perform(click())
        try {
            mainPageRobot.addedTaskItem.check(matches(Matchers.not(isDisplayed())))
        }
        catch (e: NoMatchingViewException) {
        }
    }
}