package com.justeat.androiduitechtest.tests

import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.rule.ActivityTestRule
import com.justeat.androiduitechtest.MainActivity
import com.justeat.androiduitechtest.pages.CompleteDeleteTaskPopUpRobot
import com.justeat.androiduitechtest.pages.MainPageRobot
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CheckCompletingTaskTest {
    @Rule
    @JvmField
    val mActivityRule = ActivityTestRule(MainActivity::class.java, false, false)

    val addTaskTest = AddTaskTest()
    val completeDeleteTaskPopUpRobot = CompleteDeleteTaskPopUpRobot()
    val mainPageRobot = MainPageRobot()

    @Before
    fun launchActivity(){
        mActivityRule.launchActivity(null)
    }

    @Test
    fun checkCompletingTasksAdded(){
        addTaskTest.addTaskTest()
        //mainPageRobot.clickOnItemOnTheList()
        mainPageRobot.addedTaskItem.perform(click())
        completeDeleteTaskPopUpRobot.completeTaskBtn.check(matches(isDisplayed()))
        completeDeleteTaskPopUpRobot.completeTaskBtn.perform(click())
        mainPageRobot.addedTaskItem.check(matches(not(isDisplayed())))
        mainPageRobot.completedTasksText.perform(click())
        mainPageRobot.addedTaskItem.check(matches(isDisplayed()))

    }
}