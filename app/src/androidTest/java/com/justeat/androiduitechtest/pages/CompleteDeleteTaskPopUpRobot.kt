package com.justeat.androiduitechtest.pages

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.matcher.ViewMatchers.withId
import com.justeat.androiduitechtest.R

class CompleteDeleteTaskPopUpRobot {
    val completeTaskBtn = onView(withId(R.id.task_options_complete))
    val deleteTaskBtn = onView(withId(R.id.task_options_delete))
}