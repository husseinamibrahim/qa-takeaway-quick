package com.justeat.androiduitechtest.pages

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import com.justeat.androiduitechtest.R
import org.hamcrest.Matchers.allOf

class AddTaskPopUpRobot {
    val addTaskTextField = onView(withId(R.id.add_task))
    val addTaskBtn = onView(allOf(withText("Add")))

    fun typeTextInTextView(){
        addTaskTextField.perform(click())
        addTaskTextField.perform(typeText("Test Case 1"))
    }

    fun clickOnAddBtn(){
        addTaskBtn.perform(click())
    }



}