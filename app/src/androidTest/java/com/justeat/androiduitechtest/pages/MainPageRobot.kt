package com.justeat.androiduitechtest.pages

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.v7.widget.RecyclerView
import com.justeat.androiduitechtest.R
import org.hamcrest.Matchers.allOf

/**
 * Robot approach to collect all espresso related script in on class for each page
 * All methods, objects and variables related to a certain page should be here
 */

class MainPageRobot {
    val addBtn = onView(withId(R.id.add))
    val toolBarText = onView(allOf(withText("AndroidUITechTest")))
    val activeTasksText = onView(allOf(withText("ACTIVE TASKS")))
    val completedTasksText = onView(allOf(withText("COMPLETED TASKS")))
    val recyclerViewTasksList = onView(withId(R.id.list))
    val itemElementText = "Test Case 1"
    val addedTaskItem = onView(withText(itemElementText))

    fun clickOnAddBtn(){
        addBtn.perform(click())
    }

    fun clickOnItemOnTheList(){
        recyclerViewTasksList.perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
    }

}